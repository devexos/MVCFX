package be.fpluquet.controllers;

import be.fpluquet.models.Person;
import be.fpluquet.views.PersonEditorController;
import be.fpluquet.views.PersonEditorController.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.io.IOException;

public class MainController extends Application implements PersonEditorListener {

    private Stage currentStage;

    @Override
    public void start(Stage primaryStage)  {
        currentStage = primaryStage;
        try {
            FXMLLoader loader = getPersonEditorLoader();
            Parent root = loader.load();
            PersonEditorController controller = loader.getController();
            controller.setListener(this);
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
        } catch (IOException e) {
            showErrorAlert();
        }
    }

    private FXMLLoader getPersonEditorLoader() {
        return new FXMLLoader(PersonEditorController.class.getResource("PersonEditor.fxml"));
    }

    private void showErrorAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("La fenêtre n'a pu être chargée");
        alert.setHeaderText("Un fichier nécessaire à la vue n'a pu être trouvé.");
        alert.setContentText("C'est une erreur interne. Veuillez contacter l'équipe de développement.");
        alert.showAndWait();
    }

    public static void main(String[] args) {
       launch(args);
    }

    public void onSubmitButtonClick(String firstname, String lastname) {
        Person person = new Person(firstname, lastname);
        currentStage.hide();
        try {
            FXMLLoader loader = getPersonEditorLoader();
            Parent root = loader.load();
            PersonEditorController controller = loader.getController();
            controller.setPerson(person);
            controller.setListener(new PersonEditorListener() {
                @Override
                public void onSubmitButtonClick(String firstname, String lastname) {
                    person.setFirstname(firstname);
                    person.setLastname(firstname);
                    controller.setMessageSave();
                }
            });
            currentStage = new Stage();
            currentStage.setScene(new Scene(root));
            currentStage.show();
        } catch (IOException e) {
            showErrorAlert();
        }

    }
}
