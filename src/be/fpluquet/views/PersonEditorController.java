package be.fpluquet.views;

import be.fpluquet.models.Person;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class PersonEditorController {
    @FXML
    TextField firstnameTextField;

    @FXML
    TextField lastnameTextField;

    @FXML
    Button submitButton;

    private PersonEditorListener listener;

    public void setListener(PersonEditorListener listener) {
        this.listener = listener;
    }

    public void onSubmitButtonClick(ActionEvent event) {
        String firstname = firstnameTextField.getText();
        String lastname = lastnameTextField.getText();
        listener.onSubmitButtonClick(firstname, lastname);
    }

    public void setPerson(Person person) {
        firstnameTextField.setText(person.getFirstname());
        lastnameTextField.setText(person.getLastname());
        submitButton.setText("Sauver");
    }

    public void setMessageSave() {
        submitButton.setText("Sauvé !");
    }

    public interface PersonEditorListener {
        void onSubmitButtonClick(String firstname, String lastname);
    }


}
